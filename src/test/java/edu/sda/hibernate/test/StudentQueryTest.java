package edu.sda.hibernate.test;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import javax.persistence.TypedQuery;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.Test;

import edu.sda.hibernate.domain.Address;
import edu.sda.hibernate.domain.Course;
import edu.sda.hibernate.domain.Student;
import edu.sda.hibernate.util.HibernateUtil;

public class StudentQueryTest {
	
	@Test
	public void testCreate() {
		Transaction transaction = null;
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			
			Student student = new Student();
			student.setFirstName("Lucian");
			student.setLastName("Vladimirescu");
			SimpleDateFormat format = new SimpleDateFormat("d-MMM-yyyy");
			LocalDate birthDate = format
					              .parse("12-Feb-1985")
								  .toInstant()
							      .atZone(ZoneId.systemDefault())
							      .toLocalDate();
			student.setBirthdate(birthDate);
			
			Course course = session.load(Course.class, 1l);
			student.getCourses().add(course);
			
			Address address = session.load(Address.class, 1l);
			student.setAddress(address);
			
			transaction = session.beginTransaction();
			session.save(student);
			transaction.commit();
             
       } catch (Exception ex) {
           if(transaction != null) {
        	  transaction.rollback();
           }
           System.out.println("Exceptie la creare unui obiect student " + ex.getMessage());
       }
	}

	@Test
	public void testFindAll() {
	  try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            TypedQuery<Student> query = session.createQuery("from Student", Student.class);
                       
            List<Student> students = query.getResultList();           
            System.out.println("Lista cu studenti >>> ");
            students.forEach( student -> {
            	System.out.print(student.getFirstName() + " " + student.getLastName());
            	System.out.println(" locuieste la adresa  " + student.getAddress().getStreet() + " " + student.getAddress().getCity());
            	System.out.println(" si participa la cursurile:");
            	student.getCourses().forEach(course -> {
            		System.out.print(" " + course.getCode() + " " + course.getDescription());
            	});
            });            
       } catch (Exception ex) {
           ex.printStackTrace();
       }
	}
	
	@Test
	public void testFindAll2() {
	  try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            //TypedQuery<Student> query = session.createQuery("from Student", Student.class);
                       
            Student student = session.load(Student.class, 1l);        
            System.out.println("Lista cu studenti >>> ");
            student.getCourses().forEach(course -> {
        		System.out.print(" " + course.getCode() + " " + course.getDescription());
        	});
//            students.forEach( student -> {
//            	System.out.print(student.getFirstName() + " " + student.getLastName());
//            	System.out.println(" locuieste la adresa  " + student.getAddress().getStreet() + " " + student.getAddress().getCity());
//            	System.out.println(" si participa la cursurile:");
//            	student.getCourses().forEach(course -> {
//            		System.out.print(" " + course.getCode() + " " + course.getDescription());
//            	});
//            });            
       } catch (Exception ex) {
           ex.printStackTrace();
       }
	}
}
