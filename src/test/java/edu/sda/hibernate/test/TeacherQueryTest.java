package edu.sda.hibernate.test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import org.hibernate.NonUniqueResultException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.Test;

import edu.sda.hibernate.domain.Teacher;
import edu.sda.hibernate.util.HibernateUtil;

public class TeacherQueryTest {

	@Test
	public void testCreateTeacher() {
		Transaction tx = null;
		try (Session session = HibernateUtil.getSessionFactory().openSession()){
			Teacher teacher = new Teacher();
			teacher.setFirstName("Lucian");
			teacher.setLastName("Popescu");
			teacher.setHireDate(LocalDate.now());
			
			tx = session.beginTransaction();
			session.save(teacher);
			
			tx.commit();
		} catch (Exception ex) {
			System.out.println("Exceptie creare profesor " + ex.getMessage());
			if (null != tx) {
				tx.rollback();
			}
		}
		
	}
	
	
	@Test
	public void testFindAll() {
		
		try (Session session = HibernateUtil.getSessionFactory().openSession()){
			
			try {
			TypedQuery<Teacher> query = session.createQuery(" SELECT t FROM Teacher t "
							                              + " WHERE t.firstName = :cucu "
							                              + " AND t.lastName = :lastname", Teacher.class);
			query.setParameter("cucu", "Lucian");
			query.setParameter("lastname", "Popescu");
			
			
		    Teacher teacher = query.getSingleResult();
			//Teacher teacher = session.load(Teacher.class, 1l);
			System.out.println("Teacher " + teacher.getLastName());
			} catch (NonUniqueResultException | NoResultException ex) {
				System.out.println("Fie exista mai multe resultate, fie nu exista niciun rezultat !!!");
				System.out.println(ex.getMessage());
			}
		}
		
	}
	
	
	@Test
	public void testTeacherSalaries() {
		
		try (Session session = HibernateUtil.getSessionFactory().openSession()){
			
			try {
			TypedQuery<Teacher> query = session.createQuery("SELECT t FROM Teacher t ", Teacher.class);
			
			Transaction tx = session.beginTransaction();
		    List<Teacher> teachers = query.getResultList();
		    teachers.forEach(teacher -> {
		    	teacher.setSalary(new BigDecimal((Math.random() * 100) * 1000).setScale(2, BigDecimal.ROUND_UP).doubleValue());
		    	session.saveOrUpdate(teacher);
		    });
		    tx.commit();
			
			} catch (NonUniqueResultException | NoResultException ex) {
				System.out.println("Fie exista mai multe resultate, fie nu exista niciun rezultat !!!");
				System.out.println(ex.getMessage());
			}
		}
		
	}
	
	
	@Test
	public void testAggregatedFunctions() {
		
		try (Session session = HibernateUtil.getSessionFactory().openSession()){
			
			try {
			Query query = session.createQuery(" SELECT sum(salary), avg(salary) "
					                        + " FROM Teacher t ");
			
			
		    List<Object[]> result = query.getResultList();
		    result.forEach(obj -> {
		    	 System.out.println("Suma tuturor salariilor profesorilor == " + obj[0]);
		    	 System.out.println("Media salariilor profesorilor == " + obj[1]);
		    });
		   
			
			} catch (Exception ex) {
				System.out.println(ex.getMessage());
			}
		}
		
	}
	
	
	
	
}
