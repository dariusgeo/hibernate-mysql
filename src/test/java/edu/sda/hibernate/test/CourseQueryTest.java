package edu.sda.hibernate.test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import javax.persistence.TypedQuery;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.Test;

import edu.sda.hibernate.domain.Address;
import edu.sda.hibernate.domain.Course;
import edu.sda.hibernate.domain.Student;
import edu.sda.hibernate.domain.Teacher;
import edu.sda.hibernate.util.HibernateUtil;

public class CourseQueryTest {

	@Test
	public void testCreateCourse() {
		Transaction tx = null;
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			Course course = new Course();
			course.setCode("Java123");
			course.setDescription("Java Programming for beginners");
			Teacher teacher = session.load(Teacher.class, 1l);
			course.setTeacher(teacher);

			Long studentId = createStudent(session);

			// set object in a many-to-many relationship
			Student student = session.load(Student.class, studentId);
			course.getStudents().add(student);

			tx = session.beginTransaction();
			session.save(course);

			tx.commit();
		} catch (Exception ex) {
			System.out.println("Exceptie creare curs " + ex.getMessage());
			if (null != tx) {
				tx.rollback();
			}
		}

	}

	private Long createStudent(Session session) throws ParseException {
		Student student = new Student();
		student.setFirstName("Nicu");
		student.setLastName("Gheorghe");
		SimpleDateFormat format = new SimpleDateFormat("d-MMM-yyyy");
		LocalDate birthDate = format
				              .parse("12-Feb-1985")
							  .toInstant()
						      .atZone(ZoneId.systemDefault())
						      .toLocalDate();
		 student.setBirthdate(birthDate);
		 Long addressId = createAddress(session);
         Address address = session.load(Address.class, addressId);
		// set object in a one-to-one relationship
		student.setAddress(address);
		return (Long) session.save(student);
	}

	private Long createAddress(Session session) {
		Address address = new Address();
		 address.setStreet("Campia Libertatii");
		 address.setCity("Ploiesti");
		 
		 return (long)session.save(address);
	}

	@Test
	public void testFindAll() {

		try (Session session = HibernateUtil.getSessionFactory().openSession()) {

			try {
				// TypedQuery<Course> query = session.createQuery(" SELECT c FROM Course c ", Course.class);

				// Using named query
				TypedQuery<Course> query = session.createNamedQuery("get_all", Course.class);

				List<Course> courses = query.getResultList();
				courses.forEach(course -> {
					System.out.println("Cursul " + course.getCode() + " este tinut de "
							+ course.getTeacher().getFirstName() + " " + course.getTeacher().getLastName());
				});
			} catch (Exception ex) {
				System.out.println(ex.getMessage());
			}
		}

	}

	@Test
	public void testFindTeacherByCourseCode() {

		try (Session session = HibernateUtil.getSessionFactory().openSession()) {

			try {
			   TypedQuery<Teacher> query = session.createQuery( " SELECT t "
						                        + " FROM Course c "
						                        + " JOIN c.teacher t "
					                            + " WHERE c.code = :code", Teacher.class);

				query.setParameter("code", "Java123");

				List<Teacher> result = query.getResultList();
				result.forEach(teacher -> {
					System.out.print(teacher.getFirstName() + " " + teacher.getLastName());
				});
			} catch (Exception ex) {
				System.out.println(ex.getMessage());
			}
		}
	}

	@Test
	public void testNativeQuery() {
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			try {
				Query query = session.createNativeQuery(" SELECT t.first_name, t.last_name " + 
				                                        " FROM course c " +
						                                " JOIN teacher t ON c.teacher_id = t.id" + 
				                                        " WHERE c.code = :code");

				query.setParameter("code", "Java123");

				List<Object[]> result = query.getResultList();
				result.forEach(res -> {
					System.out.println(res[0]);
					System.out.println(res[1]);
				});
			} catch (Exception ex) {
				System.out.println(ex.getMessage());
			}
		}

	}
	
	@Test
	public void testFindAll3() {
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {

              // TypedQuery<Course> query = session.createQuery("from Course", Course.class);

			
			  // N + 1 Problem Solution:
			  
			  TypedQuery<Course> query = session.createQuery("SELECT c FROM Course c JOIN FETCH c.students",
			  Course.class);
			 

			List<Course> courses = query.getResultList();
			System.out.println("Lista de cursuri >>> ");
			courses.forEach(course -> {
				System.out.print("La cursul: ");
				System.out.print(course.getCode() + " " + course.getDescription() + " vin studentii: ");
				course.getStudents()
						.forEach(student -> System.out.println(student.getFirstName() + " " + student.getLastName()));

			});
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	
	@Test
	public void testL1Cache() {
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			Course course = session.load(Course.class, 4l);
			System.out.println("Exista in sesiune ? " + session.contains(course));
			session.evict(course);
			//session.clear();
			System.out.println("Exista in sesiune ? " + session.contains(course));
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			
		}
	}

}
