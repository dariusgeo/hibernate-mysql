package edu.sda.hibernate.test;

import java.util.List;

import javax.persistence.TypedQuery;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.Test;

import edu.sda.hibernate.domain.Address;
import edu.sda.hibernate.util.HibernateUtil;

public class AddressQueryTest {

	@Test
	public void testCreate() {

		Transaction transaction = null;
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			Address address = new Address();
			address.setStreet("Lalelelor, 4");
			address.setCity("Bucuresti");

			transaction = session.beginTransaction();

			session.save(address);

			transaction.commit();
		} catch (Exception ex) {
			if (transaction != null) {
				try {
					transaction.rollback();
				} catch (Exception e) {
					ex.printStackTrace();
				}
			}
		}
	}

	@Test
	public void testFindAll() {
		try (Session session = HibernateUtil.getSessionFactory().openSession()) {
			TypedQuery<Address> query = session.createQuery("from Address", Address.class);

			List<Address> addressList = query.getResultList();
			System.out.println("Lista adrese >>> ");
			addressList.forEach(address -> System.out.println("Strada: " + address.getStreet() + " / Oras: " + address.getCity()));
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}
