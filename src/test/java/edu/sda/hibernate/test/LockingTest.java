package edu.sda.hibernate.test;

import java.time.LocalDate;

import org.hibernate.LockMode;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.Test;

import edu.sda.hibernate.domain.Teacher;
import edu.sda.hibernate.util.HibernateUtil;

public class LockingTest {

	@Test
	public void testLocking() {
		Transaction tx = null;
		try (Session session = HibernateUtil.getSessionFactory().openSession()){
			
			Teacher teacher = session.load(Teacher.class, 5l);
			teacher.setSalary(1050.50);
			session.lock(teacher, LockMode.OPTIMISTIC);
			
			tx = session.beginTransaction();
			session.save(teacher);
			
			tx.commit();
		} catch (Exception ex) {
			System.out.println("Exceptie creare profesor " + ex.getMessage());
			if (null != tx) {
				tx.rollback();
			}
		}
	}
}
