package edu.sda.hibernate.listener;

import javax.persistence.PostLoad;
import javax.persistence.PostPersist;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import edu.sda.hibernate.domain.Course;

public class CourseListener {
	
	@PostLoad
	public void postLoadMethod(Course course) {
		System.out.println("S-a incarcat entitatea ! " + course.getId());
	}
	
	@PreUpdate
	public void preUpdate(Course course) {
		System.out.println("Inainte ca entitatea sa fie salvata ! " + course.getId());
	}
	
	@PrePersist
	public void prePersist(Course course) {
		System.out.println("Se va salva entitatea (id ar trebuie sa fie null)! " + course.getId());
	}
	
	@PostPersist
	public void postPersistMethod(Course course) {
		System.out.println("Entitatea s-a salvat cu succes!! (are trebui sa existe id) " + course.getId());
	}
}
